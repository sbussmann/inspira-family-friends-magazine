##############################################################################
# Project: Claxton - Major Joint Replacement 
# Date: 5/27/14
# Author: Sam Bussmann
# Description: Combine scores and get final ranking
# Notes: Uses payer analysis from payer_type.R to get relative value
##############################################################################

### Relative value from payer_type.R analysis
medicare <- 1.074
medicaid <- .913
commercial <- 1.489
selfpay <- .048


### Calculatue expected value

expval<-(medicare*pred.payer[,"Medicare",]+medicaid*pred.payer[,"Medicaid",]+
        commercial*pred.payer[,"Commercial",]+selfpay*pred.payer[,"Self Pay",])-1

### Get CommunityPersonID, Expected Value, Propensity Score together

tog<-data.frame(CommunityPersonID=pm$CommunityPersonID,
                CommunityHouseholdID=pm$CommunityHouseholdID,Zipcode=pm[,"zipcode"],
                prediction=as.numeric(pred.prop),expmargin=expval,predcost=as.numeric(exp(pred.cost)),
                score=as.numeric(pred.prop*expval*exp(pred.cost)))

save(tog,file="combined_score_df.RData")
#load("combined_score_df.RData")


### Get CommunityHouseholiD, and dedup by that


library(RODBC)
sql<-odbcConnect("IrmSqls")

system.time(
  addr<-as.data.frame(sqlQuery(sql,
                               "SELECT 
                              [AddressID],
                              [CommunityHouseholdID]
                              FROM [IRMDm_SouJerHea64].[dbo].[CommunityHousehold] 
                              "
                               ,errors=T,stringsAsFactors = F))
)

tog_1<-merge(tog,addr,by=c("CommunityHouseholdID"),all.x=T)

tog_2<-tog_1[order(tog_1$AddressID,tog_1$score,decreasing=T),]
tog_3<-tog_2[!duplicated(tog_2$AddressID),]
save(tog_3,file="final_score.RData")
#load("final_score.RData")

### DRG selection

plot(tog_3$prediction,tog_3$expval,pch=".",
     main="Plot of Propensity Score against Expected Profit",
     xlab="Propensity Predition",ylab="Expected Profit")
hist(tog_3$score,xlim=c(0,50),breaks=100,main="Histogram of Final Score")
mean((tog_3$score>0))

x<-seq(0,.5,.01)
y<-seq(0,500,50)
persp(x,y,outer(x,y),phi=5,theta=-10)

## Sum of score by decile
b<-10
qq<-unique(quantile(tog_3$score, 
                    probs=seq.int(0,1, length.out=b+1)))
out <- cut(tog_3$score, breaks=qq, include.lowest=TRUE, labels=as.character(c(10:1)))

a<-by(tog_3$score,list(out),sum)
bb<-by(tog_3$score,list(out),function(x) sum(!is.na(x)))

decile<-data.frame(decile=names(a),N=as.numeric(bb),sum_score=round(as.numeric(a),0),
                   ave_score=round(as.numeric(a)/as.numeric(bb),2),
                   lift=100*(round((as.numeric(a)/as.numeric(bb))/mean(tog_3$score),3)-1))

library(gtools)
decile<-decile[mixedorder(decile$decile),]


write.csv(decile,file="decile.csv",quote=F,row.names=F)
save(decile,file="decile.Rdata")

par(mfrow=c(1,1))
barplot(decile[,5],names.arg=as.character(1:10),col="lightblue",xlab="Decile",
        ylab="Percent Better than Average",
        main="Lift Chart for Inspira Friends And Family Magazine",ylim=c(-110,560))

