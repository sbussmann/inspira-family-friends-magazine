##############################################################################
# Project: Inspira Family & Friends Magazine Mail List
# Date: 5/30/14
# Author: Sam Bussmann
# Description: Payer Type Analysis
# Notes: 
##############################################################################

table(pts$PatientType,pts$facility)

## Identify the codes of interest

analyze.payer(pts[pts$pt_flag==1,])

#### Establish relative value of different payer groups (really need to know the breakeven payment)
#### Ave payment by payer group

pts_ed<-pts[(pts$pt_flag==1),]

pts_ed$group[(pts_ed$PayerType %in% 
            c("Blue Cross","Managed Care","Other","Commercial","No Fault/Workers Comp"))]<-"Commercial"
pts_ed$group[(pts_ed$PayerType %in% 
            c("Medicaid","Medicaid Mgd Care"))]<-"Medicaid"
pts_ed$group[(pts_ed$PayerType %in% 
            c("Medicare"))]<-"Medicare"
pts_ed$group[(pts_ed$PayerType %in% 
            c("Indigent Care","Employee"))]<-"Self Pay"

Payer_Analysis<-analyze.payer(pts_ed,"group")
Payer_Analysis

save(Payer_Analysis,file="Payer_Analysis.RData")
#load("Payer_Analysis.RData")
write.csv(Payer_Analysis,file="Payer_Analysis.csv",row.names=F)
